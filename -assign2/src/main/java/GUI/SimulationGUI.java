//backup
package GUI;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class SimulationGUI extends JFrame{
	private static int[] nrClient;
	private static String[][] clients;
	private static JPanel panel=new JPanel();
	private static JScrollPane[] pane;
	private static JList[] list;
	public SimulationGUI(int queues) {
		clients=new String[queues][];
		nrClient=new int[queues];
		panel.setLayout(null);
		pane=new JScrollPane[queues];
		list=new JList[queues];
		for(int i=0;i<queues;i++) {
			clients[i]=new String[100];
			list[i]=new JList<String>(clients[i]);
			pane[i]=new JScrollPane(list[i]);
			pane[i].setBounds(20+i*100,30,60,200);
			panel.add(pane[i]);
		}
		this.setSize(500, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(panel);
		this.setVisible(true);
	}
	public static void addClient(int index) {
		panel.remove(pane[index]);
		clients[index][nrClient[index]++]="Client";
		list[index]=new JList<String>(clients[index]);
		pane[index]=new JScrollPane(list[index]);
		pane[index].setBounds(20+index*100,30,60,200);
		panel.add(pane[index]);
		panel.revalidate();
	}
	public static void removeClient(int index) {
		panel.remove(pane[index]);
		clients[index][--nrClient[index]]="";
		list[index]=new JList<String>(clients[index]);
		pane[index]=new JScrollPane(list[index]);
		pane[index].setBounds(20+index*100,30,60,200);
		panel.add(pane[index]);
		panel.revalidate();
	}
	

}

