package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Logic.Control;

public class SetupGUI extends JFrame{
	private JTextField[] tf=new JTextField[6];
	private JButton start;
	private JPanel panel = new JPanel();
	public SetupGUI() {
		String[] text= {"Min arrive","Max arrive","Min process","Max process","Simulation time","Number Queues"};
		for(int i=0;i<6;i++) {
			tf[i]=new JTextField(text[i]);
			panel.add(tf[i]);
		}
		start=new JButton("Start");
		panel.add(start);
		start.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Thread t;
				Control c=new Control(Integer.valueOf(tf[0].getText()),Integer.valueOf(tf[1].getText()),Integer.valueOf(tf[2].getText()),Integer.valueOf(tf[3].getText())
						,Integer.valueOf(tf[4].getText()),Integer.valueOf(tf[5].getText()));
				t=new Thread(c);
				t.start();
				new SimulationGUI(Integer.valueOf(tf[5].getText()));
			}
			
		});
		this.setSize(600,600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(panel);
		this.setVisible(true);
	}
}
