package Logic;

public class Client {
	private int id;
	private int arriveTime;
	private int processingTime;
	
	public Client(int id, int arriveTime, int processingTime) {
		this.id = id;
		this.arriveTime = arriveTime;
		this.processingTime = processingTime;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArriveTime() {
		return arriveTime;
	}
	public void setArriveTime(int arriveTime) {
		this.arriveTime = arriveTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	
	

}
