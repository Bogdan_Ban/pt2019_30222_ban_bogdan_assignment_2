package Logic;

import java.util.LinkedList;

import GUI.SimulationGUI;

public class Queue implements Runnable{
	private int index;
	private LinkedList<Client> clients=new LinkedList<>();
	//private int time=0;
	public Queue(int index) {
		this.index=index;
	}
	public void addClient(Client c) {
		System.out.println("Q:"+index+" Client"+c.getId()+" arrived at "+Control.time);
		SimulationGUI.addClient(index);
		clients.add(c);
	}
	public void removeClients(Client c) {
		SimulationGUI.removeClient(index);
		clients.remove(c);
	}
	
	public synchronized void run() {
		while(true) {
		if(clients.isEmpty())
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		else
		{
			Client c=clients.getFirst();
			System.out.println("Q:"+index+" Client"+c.getId()+" is being processed at time: "+Control.time);
			while(c.getProcessingTime()>0) {
				try {
					this.wait();
					c.setProcessingTime(c.getProcessingTime()-1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Q:"+index+" Client"+c.getId()+" finished at time: "+Control.time);
			removeClients(c);
		}
		}
	}
	
	
	public int getClientsSize() {
		return clients.size();
	}

}

