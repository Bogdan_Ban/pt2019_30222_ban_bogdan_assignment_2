package Logic;

import java.util.ArrayList;

public class Control implements Runnable{
	private int minArrive;
	private int maxArrive;
	private int minProcess;
	private int maxProcess;
	private int simulationTime;
	private int queueNr;
	public static int time=0;
	private int id=0;
	private ArrayList<Queue> queues=new ArrayList<Queue>();
	private ArrayList<Thread> queuesThread=new ArrayList<Thread>();
	public Control(int minArrive, int maxArrive, int minProcess, int maxProcess, int simulationTime, int queueNr) {
		this.minArrive = minArrive;
		this.maxArrive = maxArrive;
		this.minProcess = minProcess;
		this.maxProcess = maxProcess;
		this.simulationTime = simulationTime;
		this.queueNr = queueNr;
		for(int i=0;i<queueNr;i++) {
			queues.add(new Queue(i));
		}
		for(Queue q:queues) {
			queuesThread.add(new Thread(q));
		}
		for(Thread t:queuesThread) {
			t.start();
		}
	}
	public synchronized void run() {
		while(time<simulationTime) {
			int nextArrive=time + minArrive + (int)(Math.random()*(maxArrive-minArrive));
			int nextProcess=minProcess + (int) (Math.random())*(maxProcess-minProcess);
			while(time<nextArrive) {
				try {
					for(Queue q:queues) {
						synchronized(q) {
							q.notify();
						}
					}
					this.wait(1000);
					time++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			Client c=new Client(id++,nextArrive,nextProcess);
			Queue minQ=queues.get(0);
			for(Queue q:queues) {
				if(q.getClientsSize()<minQ.getClientsSize()) {
					minQ=q;
				}
			}
			minQ.addClient(c);
		}
	}
	
	
	
	
}
